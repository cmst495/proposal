import Vue from 'vue'
import articleTitle from './article-title.vue'
import articleIntro from './article-intro.vue'
import articleAppsWebMobile from './article-apps-web-mobile.vue'
import articleArchitecture from './article-architecture.vue'
import articleDesignWithCleanCode from './article-design-with-clean-code.vue'
import articleFeBeDev from './article-fe-be-dev.vue'
import articleMobileResponsive from './article-mobile-responsive.vue'
import articleProcess from './article-process.vue'
import articleWritingSample from './article-writing-sample.vue'
import articleCredits from './article-credits.vue'
import articleReflections from './article-reflections.vue'

Vue.component('article-title', articleTitle)
Vue.component('article-intro', articleIntro)
Vue.component('article-apps-web-mobile', articleAppsWebMobile)
Vue.component('article-architecture', articleArchitecture)
Vue.component('article-design-with-clean-code', articleDesignWithCleanCode)
Vue.component('article-fe-be-dev', articleFeBeDev)
Vue.component('article-mobile-responsive', articleMobileResponsive)
Vue.component('article-process', articleProcess)
Vue.component('article-writing-sample', articleWritingSample)
Vue.component('article-credits', articleCredits)
Vue.component('article-reflections', articleReflections)

const articles = [
  {component:'article-title'},
  {component:'article-intro'},
  {component:'article-design-with-clean-code'},
  {component:'article-apps-web-mobile'},
  {component:'article-architecture'},
  {component:'article-mobile-responsive'},
  {component:'article-fe-be-dev'},
  {component:'article-process'},
  {component:'article-writing-sample'},
  {component:'article-credits'},
  {component:'article-reflections'},
]

export default articles