/* flexbox struggles for certain setups, where css grid would readily resolve 
the issue. Ironically, ie and edge do not handle css grid properly because they 
were the originators, and their version conforms to the window operating system 
instead of the official css out-growth. therefore, another solution is 
necessary.

legitimately, this might be resolvable with flexbox still, but after some time 
no solution presented itself on stack overflow.

see my trouble-ticket with minimal-verifiable-examples: https://stackoverflow.com/questions/45523659/using-flex-grow-to-reserve-space-wrap-to-child-and-center-with-an-overlay
*/
export function wrap(wrapper, img) {
//  let wrapper = document.querySelector('.wrapper')
//  let img = document.querySelector('.wrapper > img')
	wrapper.style.width = 'auto'
  wrapper.style.height = 'auto'
  
  setTimeout(() => {
    let box = getObjectFitSize(true, img.width, img.height, img.naturalWidth, img.naturalHeight)
        
    wrapper.style.width = box.width + 'px'
    wrapper.style.height= box.height + 'px'
  }, 50)
}

// adapted from: https://www.npmjs.com/package/intrinsic-scale
function getObjectFitSize(contains /* true = contain, false = cover */, containerWidth, containerHeight, width, height){
    var doRatio = width / height;
    var cRatio = containerWidth / containerHeight;
    var targetWidth = 0;
    var targetHeight = 0;
    var test = contains ? (doRatio > cRatio) : (doRatio < cRatio);

    if (test) {
        targetWidth = containerWidth;
        targetHeight = targetWidth / doRatio;
    } else {
        targetHeight = containerHeight;
        targetWidth = targetHeight * doRatio;
    }

    return {
        width: targetWidth,
        height: targetHeight,
        x: (containerWidth - targetWidth) / 2,
        y: (containerHeight - targetHeight) / 2
    };
}