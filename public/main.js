import Vue from 'vue'

import App from './components/App.vue'

let app = new Vue({
  el: '#app',
  render: h => h(App)
})
app.eslint_decorated = true

console.log('note: $.scrollify is the reason for jQuery.')