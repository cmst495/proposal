//External
import Koa from 'koa'
import Router from 'koa-router'
import bodyParser from 'koa-bodyparser'
import Qs from 'koa-qs'
import sendfile from 'koa-sendfile'
import fs from 'fs'
//Internal
import config from 'config'
import handleErrors from './middlewares/handleErrors'
 
export const app = new Koa()
Qs(app) // let's give our app nice query strings
 
app.use(handleErrors)
app.use(bodyParser())

export const api_error = 'Error handling works!'
const router = new Router()
router.get('/api/error', async () => {
  throw Error(api_error)
})

const default_route = async (ctx, next) => {
  ctx.type = 'html'
  let e = Error('File Not Found')
  e.status = 404
  throw e
}

router.get('/', async (ctx, next) => {
  await sendfile(ctx, 'dist/public/index.html')
  if(ctx.status===404) {
    let e = new Error('File not found')  
    e.status = 404
    throw e
  }
})
router.get(/^\/media\/(.*)$/, async (ctx, next) => {
  let name = ctx.params[0]
  let ext = name.split('.').slice(-1)[0] 
  switch (ext) {
    case 'png':
      ctx.type='image/png'
      break
    case 'jpg':
    case 'jpeg':
      ctx.type='image/jpeg'
      break
    case 'otf':
      ctx.type='font/opentype'
      break
  }
  ctx.body = fs.createReadStream('public/media/'+ctx.params[0])
})
router.get(/^\/(?:(?!media\/))(.*)$/, async (ctx, next) => {
  let fspath = ctx.params[0] || 'index.html'
  let fh = config.public + '/' + fspath
  let stats = await sendfile(ctx, fh)
  console.log(ctx.status, fh)
  if(ctx.status===404) {
    let e = new Error('File not found')  
    e.status = 404
    e.errors = fh
    throw e
  }
})

app.use(router.routes())
app.use(default_route)

let server
export const start = function () {
  server = app.listen(config.port(), () => {
    console.info(`Listening to http://localhost:${config.port()}`)
  })
  return server
}
export const end = function () {
  server.close()
}