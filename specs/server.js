import test from 'tape'
import tapeSpec from 'tap-spec'
import supertest from 'supertest'
import colors from 'colors'
import config from 'config'
import 'leaked-handles'

import {app, api_error, start, end} from '../dist/server.js'

console.log('[TEST] server.js'.black.bgYellow)

process.env['NODE_ENV'] = 'TEST'

let server;
let request;

test.createStream()
  .pipe(tapeSpec())
  .pipe(process.stdout)

test('Start server', t => {
    server = start()
    request = supertest(server);
    t.end();
})

test('Koa test', t => {
    request
        .get('/api/error')
        .expect(500)
        .end((err, res) => {
            if (err) throw err;

            const api_eo = JSON.parse(res.text)

            t.equals(api_eo.message, api_error, '[/api/error] message should match the constant from the server definition.');
            
            t.end();
        });
});

test('Shutdown server', t => {
    end();
    t.end();
})