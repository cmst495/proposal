const autoprefixer = require('autoprefixer')
module.exports = {
  // provide your own postcss plugins 
  postcss: [require('autoprefixer')({
    browsers: ['last 2 versions']
  })]
}